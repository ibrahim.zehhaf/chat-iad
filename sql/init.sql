DROP TABLE IF EXISTS message;
CREATE TABLE message (id INT AUTO_INCREMENT NOT NULL, sender INT NOT NULL, recipient INT NOT NULL, content LONGTEXT NOT NULL, created DATETIME NOT NULL,  PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;

DROP TABLE IF EXISTS user;
CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, password VARCHAR(255) NOT NULL, connected TINYINT(1) DEFAULT NULL, UNIQUE INDEX UNIQ_USENAME (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;

ALTER TABLE message ADD CONSTRAINT FK_SENDER FOREIGN KEY (sender) REFERENCES user (id);
ALTER TABLE message ADD CONSTRAINT FK_RECIPIENT FOREIGN KEY (recipient) REFERENCES user (id);

INSERT INTO user (username, password, connected) values('ibrahim', '$2y$10$Fs30ZDZsiipjl.j6hGpgreT2vT2oH3QaONVTFiQth3m9c5O1StXaC', 0);
INSERT INTO user (username, password, connected) values('thomas', '$2y$10$Fs30ZDZsiipjl.j6hGpgreT2vT2oH3QaONVTFiQth3m9c5O1StXaC', 0);
INSERT INTO user (username, password, connected) values('dupont', '$2y$10$Fs30ZDZsiipjl.j6hGpgreT2vT2oH3QaONVTFiQth3m9c5O1StXaC', 0);

INSERT INTO message (sender, recipient, content, created) values(1, 2, 'Bonjour Thomas, ça va ?', '2019-09-10 23:00:00');
INSERT INTO message (sender, recipient, content, created) values(2, 1, 'Oui très bien merci', '2019-09-10 23:01:00');
INSERT INTO message (sender, recipient, content, created) values(2, 1, 'ça été le test du chat ?', '2019-09-10 23:02:00');
INSERT INTO message (sender, recipient, content, created) values(1, 2, 'Hard, j\'ai du révisé pas mal de choses sur PHP ;)', '2019-09-10 23:01:00');
INSERT INTO message (sender, recipient, content, created) values(1, 2, 'depuis qu\'ont code avec des frameworks on fait plus de PHP :D', '2019-09-10 23:02:00');
INSERT INTO message (sender, recipient, content, created) values(1, 2, 'En tout cas beau chalenge pour moi', '2019-09-10 23:02:00');
INSERT INTO message (sender, recipient, content, created) values(1, 3, 'Coucou!', '2019-09-10 23:01:00');
