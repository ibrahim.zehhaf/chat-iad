<?php

namespace Core;

use App\Model\User;use App\Repository\UserRepository;

/**
 * @author ibrahim
 */
class BaseController
{
    /**
     * Render view
     * @param $view
     * @param array $data
     */
    public function render($view, $data = null)
    {
        require dirname(__DIR__) . "/src/View/$view.php";
    }

    /**
     * Get the connected user
     * @return User
     */
    public function getUser()
    {
        $userRepository = new UserRepository();
        return $userRepository->loadUser($_SESSION["username"]);
    }
}
