<?php

namespace Core;
use App\Service\Security;

/**
 * Request dispatcher
 * @author ibrahim
 */
class Dispatcher
{

    public static function run()
    {
        session_start();
        Security::runFirewall();

        $uri = explode('/', strtok($_SERVER['REQUEST_URI'], '?'));
        $uri = array_slice($uri, 1);

        if (empty($uri[0])) {
            $uri[0] = "index";
        }

        if (empty($uri[1])) {
            $uri[1] = "index";
        }

        $controllerName = "App\\Controller\\" . ucfirst(strtolower($uri[0]));
        $action = strtolower($uri[1]);

        if (!class_exists($controllerName) || !method_exists($controllerName, $action)) {
            header("HTTP/1.0 404 Not Found");
            echo "404 NOT FOUND";
            exit;
        }

        $params = [
            'get' => $_GET,
            'post' => $_POST
        ];

        $controller = new $controllerName;
        $controller->$action($params);
    }
}
