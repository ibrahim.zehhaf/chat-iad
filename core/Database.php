<?php
namespace Core;

/**
 * @author ibrahim
 */
class Database
{

    /**
     * @var \PDO
     */
    private static $conn;

    public function __construct()
    {
        if (null === self::$conn) {
            $host = DB_HOST;
            $dbname = DB_NAME;
            self::$conn = new \PDO("mysql:host=$host;dbname=$dbname", DB_USER, DB_PASSWORD);
            // more secure sql to prevent injection with emulated prepares
            self::$conn->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
            self::$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        }
    }

    public function query($sql)
    {
        return self::$conn->prepare($sql);
    }
}
