#!/bin/bash
set -e

docker-compose kill && docker-compose rm -f && docker-compose up -d

# wait for db up
sleep 10
docker-compose exec iad_mysql sh -c "mysql -u root -piad iad < /tmp/sql/init.sql"

echo "###################################"
echo "Install OK, Go to http://localhost:8003"
