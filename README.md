# Manuel d'installation

## Prérequis
* **Docker** et **docker-compose** doivent être installé
* Le port **8003** doit être libre

## Installation
Pour installer l'application is suffit de vous mettre dans le répertoire du projet
et  l'ancer le script **./install.sh**.

Appliquer les droits d'execution si nécessaire
```
cd /path/to/project
chmod +x ./install.sh
./install.sh
```
## Fonctionnalités
* Accèss sécurisé via login / mot de pass
* Inscription
* Déconnexion
* Affichage en temps réel des utilisatuers avec leur statut (online / offline)
* Chargement de l'historique de conversation après séléction du correspondant

## Technos
* Docker et docker-compose pour l'infra
* PDO / MySql 
* PHP 7
* Nginx
* Composer pour l'autoloading
* jQuery et bootsratp 4 pour le front
* Pattern MVC 

## Utilisation
Url : http://localhost:8003

Vous pouvez ouvrir deux fenêtres de votre navigateur dont une en navigation privée.  
Sur une des deux fenêtre connectez-vous avec le user (username = ibrahim et password = pass).  
Sur l'autre fenêtre connectez-vous avec le user (username = thomas et password = pass).  
Possibilité d'inscripition sur  http://localhost:8003/security/register

## Accès bdd si besoin
```
host: 127.0.0.1 
port: 3307 
schema: iad
user: root
password: iad
```