<?php
namespace App\Controller;

use Core\BaseController;
use App\Repository\UserRepository;

/**
 * @author ibrahim
 */
class Users extends BaseController
{

    public function load()
    {
        $userRepo = new UserRepository();
        $users = $userRepo->list();
        header('Content-Type: application/json');
        echo json_encode($users);
    }
}
