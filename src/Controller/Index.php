<?php

namespace App\Controller;

use Core\BaseController;
use App\Service\Security;

/**
 * @author ibrahim
 */
class Index extends BaseController
{

    public function index()
    {
        $token = Security::initToken();
        $user = $this->getUser();
        $this->render("chat", ["user" => $user, "token" => $token]);
    }
}
