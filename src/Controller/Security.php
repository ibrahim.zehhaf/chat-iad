<?php

namespace App\Controller;

use App\Model\User;
use App\Repository\UserRepository;
use Core\BaseController;
use App\Service\Security as SecurityService;

/**
 * Handle security
 * @author ibrahim
 */
class Security extends BaseController
{

    public function login($params)
    {
        if (isset($params["post"]["username"]) && isset($params["post"]["password"])) {
            SecurityService::checkTokenValidity();
            SecurityService::handleLogin($params["post"]["username"], $params["post"]["password"]);
            $user = $this->getUser();
            $userRepo = new UserRepository();
            $userRepo->updateStatus($user, 1);
        }

        $token = SecurityService::initToken();
        $this->render("login", ["token" => $token]);
    }

    public function logout()
    {
        $user = $this->getUser();
        SecurityService::handleLogout();
        $userRepo = new UserRepository();
        $userRepo->updateStatus($user, 0);
    }

    public function register($params)
    {
        if (isset($params["post"]["username"]) && !empty($params["post"]["username"])
            && isset($params["post"]["password"]) && isset($params["post"]["confirm_password"])) {
            //check password validity
            try {
                SecurityService::chackPasswordValidity($params["post"]["password"], $params["post"]["confirm_password"]);
            } catch (\Exception $e) {
                echo $e->getMessage();
                exit;
            }

            SecurityService::checkTokenValidity();

            $userRepo = new UserRepository();
            try {
                $user = new User();
                $user->setUsername($params["post"]["username"]);
                $user->setPassword($params["post"]["password"]);
                $userRepo->insert($user);
                header("location:/security/login");
            } catch (\Exception $e) {
                // log and properly display error to the user
                echo $e->getMessage();
            }
        }
        $token = SecurityService::initToken();
        $this->render("register", ["token" => $token]);
    }
}
