<?php

namespace App\Controller;

use App\Model\Message;
use Core\BaseController;
use App\Repository\MessageRepository;
use App\Service\Security;

/**
 * @author ibrahim
 */
class Chat extends BaseController
{
    /**
     * Send a message
     * @param $params
     */
    public function send($params)
    {
        if (!isset($params['post']['content']) || empty($params['post']['content'])
            || !isset($params['post']['recipient']) || empty($params['post']['recipient'])) {
            // log
            header('HTTP/1.1 400 Bad Request');
            echo "400 BAD REQUEST";
            exit;
        }

        Security::checkTokenValidity();

        $message = new Message($params['post']['content'], $this->getUser()->getId(), $params['post']['recipient']);
        $messageRepo = new MessageRepository();
        $messageRepo->insert($message);
    }

    /**
     * Load conversation
     * @param $params
     */
    public function load($params)
    {
        if (!isset($params['get']['recipient']) || !is_numeric($params['get']['recipient'])) {
            // log
            header('HTTP/1.1 400 Bad Request');
            echo "400 BAD REQUEST";
            exit;
        }

        $messageRepo = new MessageRepository();
        $messages = $messageRepo->getByUser($this->getUser(), $params['get']['recipient']);
        header('Content-Type: application/json');
        echo json_encode($messages);
    }
}
