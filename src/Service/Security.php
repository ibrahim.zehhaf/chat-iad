<?php

namespace App\Service;

use App\Repository\UserRepository;

class Security
{
    public static function runFirewall()
    {
        if (!isset($_SESSION['username']) && strpos($_SERVER['REQUEST_URI'], "/security") === false) {
            header("location:/security/login");
            exit;
        }
    }

    public static function handleLogin($username, $password)
    {
        $userRepository = new UserRepository();
        $user = $userRepository->loadUser($username);
        if ($user && password_verify($password, $user->getPassword())) {
            // update connected info
            $_SESSION['username'] = $username;
            header("location:/");
            session_write_close();
        }

        if (!isset($_SESSION['username'])) {
            header("location:/security/login");
            exit;
        }
    }

    public static function handleLogout()
    {
        if (session_destroy()) {
            header("Location: /security/login");
        }
    }

    public static function chackPasswordValidity($password, $confirmPassword)
    {
        if ($password !== $confirmPassword) {
            throw new \Exception("The passwords are not identical");
        }

        if (!preg_match("#(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$#", $password)) {
            throw new \Exception("The passwords d'ont match the requested format");
        }
    }

    public static function checkTokenValidity()
    {
        if (!isset($_POST['token']) AND empty($_POST['token'])) {
            header('HTTP/1.1 400 Bad Request');
            echo "400 BAD REQUEST";
            exit;
        }

        if ($_SESSION['token'] !== $_POST['token']) {
            header('HTTP/1.1 400 Bad Request');
            echo "400 BAD REQUEST";
            exit;
        }
    }

    public static function initToken()
    {
        if(!isset($_SESSION['token']) || empty($_SESSION['token']) ){
            $token = bin2hex(random_bytes(20));
            $_SESSION['token'] = $token;
        }

        return $_SESSION['token'];
    }

}