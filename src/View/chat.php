<!DOCTYPE html>
<html>
<head>
    <title>IAD Chat</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">
    <link rel="stylesheet" href="css/chat.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="/">Chat IAD</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/security/logout">Logout</a>
            </li>
        </ul>
        <span class="navbar-text">
            Bonjour <?php echo $data["user"]->getUsername(); ?>
        </span>
    </div>
</nav>

<div class="container p-4">
    <div class="row">
        <div class="col-3">
            <div class="users">
                <h3>User list</h3>
                <ul></ul>
            </div>
        </div>
        <div class="col-9">
            <form id="chat-form" action="chat/send" method="post">
                <div class="form-group">
                    <h3 class="selected-user"></h3>
                    <h4>Messages</h4>
                    <div class="messages">
                        <ul></ul>
                    </div>
                    <textarea type="text" class="form-control" name="content" disabled></textarea>
                    <input type="hidden" name="recipient" id="recipient">
                    <input type="hidden" name="token" value="<?php echo $data["token"] ?>">
                </div>
                <button type="submit" class="btn btn-primary">Send</button>
            </form>
        </div>
    </div>
</div>
<script src="/js/jquery-3.4.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="/js/chat.js"></script>
</body>
</html>