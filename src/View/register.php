<!DOCTYPE html>
<html>
<head>
    <title>Register | IAD Chat</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="/">Chat IAD</a>
    <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/security/login">Login</a>
            </li>
        </ul>
    </div>
</nav>

<div class="container p-4">
    <div class="w-50 m-auto">
        <h3>Register to join IAD Chat</h3>
        <form action="/security/register" method="post">
            <div class="form-group">
                <label for="username">Login</label>
                <input type="text" name="username" class="form-control" id="username" required>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" name="password" class="form-control" id="password" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" required>
                <small id="emailHelp" class="form-text text-muted">UpperCase, LowerCase, Number/SpecialChar and min 8 Chars"</small>
            </div>
            <div class="form-group">
                <label for="confirm_password">Confirm password</label>
                <input type="password" name="confirm_password" class="form-control" id="confirm_password" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" required>
            </div>
            <input type="hidden" name="token" value="<?php echo $data["token"] ?>">
            <button type="submit" class="btn btn-primary">Register</button>
        </form>
    </div>
</div>
<script src="/js/jquery-3.4.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="/js/register.js"></script>
</body>
</html>