<?php

namespace App\Repository;

use App\Model\Message;
use App\Model\User;
use Core\Database;

/**
 * @author ibrahim
 */
class MessageRepository
{

    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    /**
     * Get user conversation
     * @param User $sender
     * @param $recipient
     * @return array
     */
    public function getByUser(User $sender, $recipient)
    {
        $stmt = $this->db->query("SELECT * FROM message WHERE (sender = :sender1 AND recipient = :recipient1) OR (sender = :recipient2 AND recipient = :sender2)");
        $stmt->execute([
            ':sender1'=> $sender->getId(),
            ':sender2'=> $sender->getId(),
            ':recipient1'=> (int)$recipient,
            ':recipient2'=> (int)$recipient,
        ]);

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Insert new message
     * @param Message $message
     */
    public function insert(Message $message)
    {
        $stmt = $this->db->query("INSERT INTO message (content, sender, recipient, created) VALUES (?,?,?,?)");
        $stmt->execute([$message->getContent(), $message->getSender(), $message->getRecipient(), $message->getCreated()]);
    }
}
