<?php

namespace App\Repository;

use App\Model\User;use Core\Database;

/**
 * @author ibrahim
 */
class UserRepository
{

    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function list()
    {
        $stmt = $this->db->query("SELECT id, username, connected FROM user");
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @param $username
     *
     * @return User
     */
    public function loadUser($username)
    {
        $stmt = $this->db->query("SELECT id, username, password FROM user WHERE username = :username");
        $stmt->bindParam(':username', $username);
        $stmt->execute();
        return $stmt->fetchObject(User::class);
    }

    public function updateStatus(User $user, $connected)
    {
        $stmt = $this->db->query("UPDATE user SET connected = :connected WHERE username = :username");
        $stmt->bindParam(':username', $user->getUsername());
        $stmt->bindParam(':connected', $connected);
        $stmt->execute();
    }

    public function insert(User $user)
    {
        $stmt = $this->db->query("INSERT INTO user (username, password) VALUES (?,?)");
        $stmt->execute([$user->getUsername(), password_hash($user->getPassword(), PASSWORD_BCRYPT)]);
    }
}
