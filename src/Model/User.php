<?php

namespace App\Model;

class User
{

    private $id;
    private $username;
    private $password;
    private $connected = 0;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    function getUsername()
    {
        return $this->username;
    }

    function setUsername($username)
    {
        $this->username = strip_tags($username);
        return $this;
    }

    function getPassword()
    {
        return $this->password;
    }

    function setPassword($password)
    {
        $this->password = strip_tags($password);
        return $this;
    }

    function isConnected()
    {
        return $this->connected;
    }

    function setConnected($connected)
    {
        $this->connected = $connected;
        return $this;
    }
}
