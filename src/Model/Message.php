<?php

namespace App\Model;

class Message
{

    private $content;
    private $sender;
    private $recipient;
    private $created;

    public function __construct($content, $sender, $recipient)
    {
        $this->sender = $sender;
        $this->recipient = strip_tags($recipient);
        $this->content = strip_tags($content);
        $this->created = (new \DateTime())->format("Y-m-d H:i:s");
    }

    function getContent()
    {
        return $this->content;
    }

    function getCreated()
    {
        return $this->created;
    }

    function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @param mixed $sender
     * @return Message
     */
    public function setSender($sender)
    {
        $this->sender = $sender;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * @param mixed $recipient
     * @return Message
     */
    public function setRecipient($recipient)
    {
        $this->recipient = $recipient;
        return $this;
    }
}
