<?php
/**
 * Main controller
 * @author ibrahim
 */
require dirname(__DIR__) . '/vendor/autoload.php';
require dirname(__DIR__) . '/config/db.conf.php';

Core\Dispatcher::run();
