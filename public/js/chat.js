
var selectedRecipient = null;
var users = [];

// submit message handling
$("#chat-form").submit(function (e) {
    e.preventDefault();
    let form = $(this);
    let url = form.attr('action');
    let data = form.serializeArray();

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function (res)
        {
            $("textarea").val("");
            $('textarea').focus();
            loadConversation();
        }
    });
});

function loadConversation() {
    if (selectedRecipient === null) {
        return;
    }

    $.ajax({
        type: "GET",
        url: "chat/load",
        data: {"recipient": selectedRecipient.id},
        success: function (res)
        {
            $(".messages ul li").remove();
            $.each(res, function (i, message) {
                $(".messages ul").append('<li>' + '<span class="text-muted">[' + message.created + '] <b>' + users[message.sender] + '</b> said : </span>' + message.content + '</li>');
            });
            $(".messages").scrollTop($(".messages")[0].scrollHeight);
        }
    });
}

function loadUsers() {
    $.ajax({
        type: "GET",
        url: "users/load",
        success: function (res)
        {
            $(".users ul li").remove();
            $.each(res, function (i, user) {
                users[user.id] = user.username;

                let li = '<li><a href="" data-id="' + user.id + '">' + user.username
                        + '</a> <span class="badge badge-' + (user.connected === 1 ? 'success' : 'secondary') + '">'
                        + (user.connected === 1 ? 'on' : 'off') + '</span>'
                        + '</li>';

                $(".users ul").append(li);
            });

            $(".users li a").click(function (e) {
                e.preventDefault();
                let username = $(this).text();
                let id = $(this).data("id");
                $(".selected-user").text("You are talking with " + username);
                $("#recipient").val(id);
                $("textarea").removeAttr("disabled");
                selectedRecipient = {"username": username, "id": id};
                loadConversation();
            });
        }
    });
}

loadUsers();

setInterval(loadUsers, 3000);
setInterval(loadConversation, 1000);

